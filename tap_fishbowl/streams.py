"""Stream type classes for tap-fishbowl."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_fishbowl.client import fishbowlStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class ItemsStream(fishbowlStream):
    """Define custom stream."""
    name = "items_list"
    path = "/items?expand=saleItems&hideZeroQuantityItems=true"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.results[*]"
    no_pagination_advance_sale = True
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("version", th.IntegerType),
        th.Property("createdById",th.IntegerType),
        th.Property("createdBy",th.StringType),
        th.Property("lastModifiedById",th.IntegerType),
        th.Property("lastModifiedBy",th.StringType),
        th.Property("dateLastModified",th.StringType),
        th.Property("dateCreated",th.StringType),
        th.Property("deleted",th.BooleanType),
        th.Property("checkVersion",th.BooleanType),
        # th.Property("customFields",th.ArrayType),
        th.Property("name",th.StringType),
        th.Property("description",th.StringType),
        th.Property("details",th.StringType),
        th.Property("cost",th.IntegerType),
        th.Property("upc",th.StringType),
        th.Property("sku",th.StringType),
        th.Property("url",th.StringType),
        th.Property("itemType",th.StringType),
        th.Property("accountingId",th.IntegerType),
        th.Property("images",th.StringType),
        th.Property("itemUomConversions",th.StringType),
        th.Property("defaultUom",th.StringType),
        th.Property("defaultUomId",th.IntegerType),
        th.Property("salesTaxId",th.IntegerType),
        th.Property("salesTax",th.IntegerType),
        th.Property("length",th.IntegerType),
        th.Property("width",th.IntegerType),
        th.Property("height",th.StringType),
        th.Property("dimensionUnit",th.StringType),
        th.Property("weight",th.IntegerType),
        th.Property("weightUnit",th.StringType),
        th.Property("itemTrackingTypes",th.StringType),
        th.Property("itemLocations",th.StringType),
        th.Property("reorderPoints",th.StringType),
        th.Property("taxable",th.BooleanType),
        th.Property("inventoryLocationSummaries",th.ArrayType(
            th.ObjectType(
                th.Property("itemId", th.IntegerType),
                th.Property("rootLocationId", th.IntegerType),
                th.Property("totalQty", th.IntegerType),
                th.Property("committedQty", th.IntegerType),
                th.Property("qtyIncomingOnSalesOrders", th.IntegerType),
                th.Property("qtyIncomingOnPurchaseOrders", th.StringType),
                th.Property("allocatedOnSalesOrders", th.StringType),
                th.Property("allocatedOnPurchaseOrders", th.StringType),
                th.Property("availableQty", th.StringType),
                th.Property("qtyShort", th.StringType),
            )
        )),
        th.Property("alertNotes",th.StringType),
        th.Property("dateDeleted",th.StringType),
        th.Property("classId",th.StringType),
        th.Property("clazz",th.StringType)
                
        ).to_dict()



class LocationStream(fishbowlStream):
    """Define custom stream."""
    name = "/location_list"
    path = "/locations"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.results[*]"
    no_pagination_advance_sale = True
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("version", th.IntegerType),
        th.Property("dateLastModified",th.StringType),
        th.Property("dateCreated",th.StringType),
        th.Property("deleted",th.BooleanType),
        th.Property("checkVersion",th.BooleanType),
        th.Property("name",th.StringType),
        th.Property("displayName",th.StringType),
        th.Property("path",th.StringType),
        th.Property("description",th.StringType),
        th.Property("companyWide",th.BooleanType),
        th.Property("parentLocation",th.StringType),
        th.Property("parentLocationId",th.IntegerType),
        th.Property("depth",th.IntegerType),
        th.Property("heritage",th.StringType),
        th.Property("useParentAddress",th.BooleanType),
        th.Property("type",th.StringType),
        th.Property("inventoryEvents",th.StringType),
        th.Property("sortNumber",th.IntegerType),
        th.Property("totalLocationQty",th.IntegerType),
        th.Property("errors",th.StringType),
        
                
        ).to_dict()


class SalesOrderItemsStream(fishbowlStream):
    """Define custom stream."""
    name = "/sales_order_items_list"
    path = "/sales_order_items"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.results[*]"
    no_pagination_advance_sale = True
 
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("version", th.IntegerType),
        th.Property("createdById",th.IntegerType),
        th.Property("createdBy",th.StringType),
        th.Property("lastModifiedById",th.IntegerType),
        th.Property("lastModifiedBy",th.StringType),
        th.Property("dateLastModified",th.StringType),
        th.Property("dateCreated",th.StringType),
        th.Property("checkVersion",th.BooleanType),
        th.Property("lineNumber",th.IntegerType),
        th.Property("salesOrderId",th.IntegerType),
        th.Property("salesOrder",th.IntegerType),
        th.Property("saleItemId",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("description",th.StringType),
        th.Property("customerSaleItemName",th.StringType),
        th.Property("dateScheduled",th.StringType),
        th.Property("dateFulfilled",th.StringType),
        th.Property("notes",th.StringType),
        th.Property("quantity",th.IntegerType),
        th.Property("quantityFulfilled",th.IntegerType),
        th.Property("quantityPicked",th.IntegerType),
        th.Property("uomId",th.IntegerType),
        th.Property("uom",th.IntegerType),
        th.Property("price",th.IntegerType),
        th.Property("cost",th.IntegerType),
        th.Property("margin",th.IntegerType),
        th.Property("markup",th.IntegerType),
        th.Property("discountPercent",th.IntegerType),
        th.Property("discountFlatAmount",th.IntegerType),
        th.Property("discountTotal",th.IntegerType),
        th.Property("multiCurrencyDiscountTotal",th.IntegerType),
        th.Property("total",th.IntegerType),
        th.Property("multiCurrencyTotal",th.IntegerType),
        th.Property("taxTotal",th.IntegerType),
        th.Property("multiCurrencyTaxTotal",th.IntegerType),
        th.Property("salesOrderItemType",th.StringType),
        th.Property("salesOrderItemStatus",th.StringType),
        th.Property("taxId",th.IntegerType),
        th.Property("tax",th.IntegerType),
        th.Property("taxRate",th.IntegerType),
        th.Property("taxable",th.BooleanType),
        th.Property("exchangeCurrencyId",th.IntegerType),
        th.Property("exchangeCurrency",th.StringType),
        th.Property("exchangeRate",th.IntegerType),
        th.Property("channelId",th.IntegerType),
        th.Property("discountReason",th.StringType),
        th.Property("accountingClassId",th.IntegerType),
        th.Property("accountingClass",th.StringType),
        th.Property("salesOrderBundleItemId",th.IntegerType),
        
                      
        ).to_dict()


class PurchaseOrderItemsStream(fishbowlStream):
    """Define custom stream."""
    name = "/purchase_order_items_list"
    path = "/purchase_order_items"
    primary_keys = ["id"]
    replication_key = None
    records_jsonpath = "$.results[*]"
    no_pagination_advance_sale = True
 
    schema = th.PropertiesList(
        th.Property("id", th.IntegerType),
        th.Property("version", th.IntegerType),
        th.Property("createdById",th.IntegerType),
        th.Property("createdBy",th.StringType),
        th.Property("lastModifiedById",th.IntegerType),
        th.Property("lastModifiedBy",th.StringType),
        th.Property("dateLastModified",th.StringType),
        th.Property("dateCreated",th.StringType),
        th.Property("checkVersion",th.BooleanType),
        th.Property("itemId",th.StringType),
        th.Property("item",th.ObjectType(
            th.Property("id",th.IntegerType),
            th.Property("version",th.IntegerType),
            th.Property("createdById",th.IntegerType),
            th.Property("createdBy",th.StringType),
            th.Property("lastModifiedById",th.IntegerType),
            th.Property("lastModifiedBy",th.StringType),
            th.Property("dateLastModified",th.StringType),
            th.Property("dateCreated",th.StringType),
            th.Property("deleted",th.BooleanType),
            th.Property("checkVersion",th.BooleanType),
            th.Property("name",th.StringType),
            th.Property("description",th.StringType),
            th.Property("details",th.StringType),
            th.Property("cost",th.IntegerType),
            th.Property("upc",th.StringType),
            th.Property("sku",th.StringType),
            th.Property("url",th.StringType),
            th.Property("itemType",th.StringType),
            th.Property("accountingId",th.StringType),
            th.Property("inventoryEvents",th.StringType),
            th.Property("images",th.StringType),
            th.Property("defaultUomId",th.IntegerType),
            th.Property("salesTaxId",th.IntegerType),
            th.Property("salesTax",th.IntegerType),
            th.Property("length",th.IntegerType),
            th.Property("width",th.IntegerType),
            th.Property("height",th.IntegerType),
            th.Property("dimensionUnit",th.StringType),
            th.Property("weight",th.IntegerType),
            th.Property("weightUnit",th.StringType),
            th.Property("itemTrackingTypes",th.StringType),
            th.Property("saleItems",th.StringType),
            th.Property("itemLocations",th.StringType),
            th.Property("reorderPoints",th.IntegerType),
            th.Property("taxable",th.BooleanType),
            th.Property("itemSubstitutes",th.StringType),
            th.Property("substituteItems",th.StringType),
            th.Property("assetAccount",th.IntegerType),
            th.Property("assetAccountId",th.IntegerType),
            th.Property("scrapAccount",th.IntegerType),
            th.Property("scrapAccountId",th.IntegerType),
            th.Property("varianceAccount",th.StringType),
            th.Property("varianceAccountId",th.StringType),
            th.Property("costOfGoodsSoldAccount",th.StringType),
            th.Property("costOfGoodsSoldAccountId",th.IntegerType),
            th.Property("adjustmentAccount",th.StringType),
            th.Property("adjustmentAccountId",th.StringType),
            th.Property("errors",th.StringType),
            th.Property("tags",th.StringType),
            th.Property("vendorItems",th.StringType),
            th.Property("countryOfOrigin",th.StringType),
            th.Property("tariffNumber",th.StringType),
            th.Property("inventoryLocationSummaries",th.StringType),
            th.Property("alertNotes",th.StringType),
            th.Property("dateDeleted",th.StringType),
            th.Property("classId",th.IntegerType),
            th.Property("clazz",th.StringType)

        )),
        th.Property("lineNumber",th.IntegerType),
        th.Property("name",th.StringType),
        th.Property("description",th.StringType),
        th.Property("purchaseOrderItemType",th.StringType),
        th.Property("purchaseOrderItemStatus",th.StringType),
        th.Property("vendorItemName",th.StringType),
        th.Property("customerId",th.IntegerType),
        th.Property("customer",th.StringType),
        th.Property("quantity",th.IntegerType),
        th.Property("quantityReceived",th.IntegerType),
        th.Property("quantityFulfilled",th.IntegerType),
        th.Property("quantityPicked",th.IntegerType),
        th.Property("notes",th.StringType),
        th.Property("uomId",th.ObjectType(
            th.Property("id", th.IntegerType),
            th.Property("version", th.IntegerType),
            th.Property("createdById", th.IntegerType),
            th.Property("createdBy", th.StringType),
            th.Property("lastModifiedById", th.IntegerType),
            th.Property("lastModifiedBy", th.StringType),
            th.Property("dateLastModified", th.StringType),
            th.Property("dateCreated", th.StringType),
            th.Property("deleted", th.BooleanType),
            th.Property("checkVersion", th.BooleanType),
            th.Property("name", th.StringType),
            th.Property("description", th.StringType),
            th.Property("abbreviation", th.StringType),
            th.Property("isIntegral", th.BooleanType),
            th.Property("defaultFlag", th.BooleanType),
            th.Property("fromConversions", th.StringType),
            th.Property("systemUom", th.BooleanType),
            th.Property("dateDeleted", th.StringType),
            th.Property("unitCost", th.IntegerType),
            th.Property("multiCurrencyUnitCost", th.StringType),
            th.Property("dateScheduled", th.StringType),
            th.Property("dateFulfilled", th.StringType),
            th.Property("purchaseOrderId", th.IntegerType),
            th.Property("purchaseOrder", th.ObjectType(
                th.Property("id", th.IntegerType),
                th.Property("version", th.IntegerType),
                th.Property("createdById", th.IntegerType),
                th.Property("createdBy", th.StringType),
                th.Property("lastModifiedById", th.IntegerType),
                th.Property("lastModifiedBy", th.StringType),
                th.Property("dateLastModified", th.StringType),
                th.Property("dateCreated", th.StringType),
                th.Property("deleted", th.BooleanType),
                th.Property("checkVersion", th.BooleanType),
                th.Property("vendorId", th.IntegerType),
                th.Property("vendor", th.ObjectType(
                    th.Property("id", th.IntegerType),
                    th.Property("version", th.IntegerType),
                    th.Property("createdById", th.IntegerType),
                    th.Property("createdBy", th.StringType),
                    th.Property("lastModifiedById", th.IntegerType),
                    th.Property("lastModifiedBy", th.StringType),
                    th.Property("dateLastModified", th.StringType),
                    th.Property("dateCreated", th.StringType),
                    th.Property("deleted", th.BooleanType),
                    th.Property("checkVersion", th.BooleanType),
                    th.Property("name", th.StringType),
                    th.Property("phone", th.StringType),
                    th.Property("fax", th.StringType),
                    th.Property("email", th.StringType),
                    th.Property("cc", th.StringType),
                    th.Property("bcc", th.StringType),
                    th.Property("other", th.StringType),
                    th.Property("url", th.StringType),
                    th.Property("taxIdentifier", th.StringType),
                    th.Property("acctNum", th.StringType),
                    th.Property("notes", th.StringType),
                    th.Property("onlyShowVendorItemsOnPo", th.BooleanType),
                    th.Property("errors", th.StringType),
                    th.Property("paymentTermId", th.IntegerType),
                    th.Property("paymentTerm", th.ObjectType(
                        th.Property("id", th.IntegerType),
                        th.Property("version", th.IntegerType),
                        th.Property("createdById", th.IntegerType),
                        th.Property("createdBy", th.StringType),
                        th.Property("lastModifiedById", th.IntegerType),
                        th.Property("lastModifiedBy", th.StringType),
                        th.Property("dateLastModified", th.StringType),
                        th.Property("dateCreated", th.StringType),
                        th.Property("deleted", th.BooleanType),
                        th.Property("checkVersion", th.BooleanType),
                        th.Property("name", th.StringType),
                        th.Property("dueDays", th.IntegerType),
                        th.Property("dayOfMonthDue", th.StringType),
                        th.Property("graceDays", th.StringType),
                        th.Property("discountDays", th.StringType),
                        th.Property("discountPercent", th.IntegerType),
                        th.Property("paymentTermType", th.StringType),
                        th.Property("accountingId", th.IntegerType),
                        th.Property("dateDeleted", th.StringType)
                    )),
                    th.Property("accountingId", th.IntegerType),
                    th.Property("vendorAddresses", th.StringType),
                    th.Property("taxId", th.StringType),
                    th.Property("tax", th.StringType),
                    th.Property("currencyId", th.IntegerType),
                    th.Property("currency", th.ObjectType(

                        th.Property("id", th.IntegerType),
                        th.Property("version", th.IntegerType),
                        th.Property("createdById", th.IntegerType),
                        th.Property("createdBy", th.StringType),
                        th.Property("lastModifiedById", th.IntegerType),
                        th.Property("lastModifiedBy", th.StringType),
                        th.Property("dateLastModified", th.StringType),
                        th.Property("dateCreated", th.StringType),
                        th.Property("deleted", th.BooleanType),
                        th.Property("checkVersion", th.BooleanType),
                        th.Property("name", th.StringType),
                        th.Property("code", th.StringType),
                        th.Property("exchangeRate", th.IntegerType),
                        th.Property("isHomeCurrency", th.BooleanType),
                        th.Property("setManually", th.BooleanType),
                        th.Property("dateDeleted", th.StringType),
                    )),

                )),
                th.Property("locationId", th.IntegerType),
                # th.Property("locationId", th.IntegerType),
                
            )),
            
        ))

                      
        ).to_dict()


